from os import name
import sqlite3
import hashlib
from getpass import getpass

def create():
    name = input("Entrez votre nom: ")
    email = input("Entrez votre email: ")
    password = hashlib.sha256(getpass("Entrez votre mot de passe: ").encode("utf-8")).hexdigest()
    createUser(name, email, password)

def createUser(name, email, password):
    con = sqlite3.connect("./data/beecoin.db")
    cursor = con.cursor()
    data = (name, email, password)
    print(name)
    print(email)
    print(password)
    query = "INSERT into USERS (name,email,password,amount) VALUES(?,?,?,1000)"
    cursor.execute(query, data)
    con.commit()

def read_one():
    con = sqlite3.connect("./data/beecoin.db")
    cursor = con.cursor()
    ids = input("Entrez votre ID: ")
    print(ids)
    query = "SELECT * FROM USERS WHERE id = ?"
    result = cursor.execute(query,ids)
    if(result):
        for i in result:
            print(f"Name is: {i[1]}")
            print(f"Email is: {i[3]}")
    else:
        print("L'ID entré n'existe pas")
    cursor.close()

def read_all():
    con = sqlite3.connect("./data/beecoin.db")
    cursor = con.cursor()
    query = "SELECT * from USERS"
    result = cursor.execute(query)
    if(result):
        print("\n =========== RESULTATS ==========")
        for i in result:
            print(f"Le nom est : {i[1]}")
            print(f"Le mail est : {i[3]}")
    else:
        pass

def update():
    con = sqlite3.connect("./data/beecoin.db")
    cursor = con.cursor()
    email = input("Entrez votre email: ")
    password = hashlib.sha256(getpass("Entrez votre mot de passe: ").encode("utf-8")).hexdigest()
    data = (email,)
    query = "SELECT * FROM users WHERE email = ?"
    result = cursor.execute(query,data)
    if(result):
        for i in result:
            name = i[1]
            password_db = i[2]
            email = i[3]
    else:
        logError()
        return
    if(password == password_db):
        print(f"connecté en tant que :{name}" )

def editAccountMenu():
    print("a")

def logError():
    print("email ou mot de passe incorrect")

def updateAccount(name, email, password):
    con = sqlite3.connect("./data/beecoin.db")
    cursor = con.cursor()
    data = (name, email, password)
    query = "UPDATE USERS set name = ?, email = ?, password = ? WHERE id = ?"
    result = cursor.execute(query, data)
    con.commit()
    cursor.close()
    if(result):
        print("Mis a jour!")
    else:
        print("Erreur")

def send():
    con = sqlite3.connect("./data/beecoin.db")
    cursor = con.cursor()
    email = input("Entrez votre Email: ")
    data = (email)
    password = hashlib.sha256(getpass("Entrez votre mot de passe: ").encode("utf-8")).hexdigest()
    query = "SELECT * FROM users WHERE email = ?"
    result = cursor.execute(query, data)
    con.commit()
    result = cursor.execute(query,data)
    if(result):
        for i in result:
            name = i[1]
            password_db = i[2]
            email = i[3]
    else:
        logError()
        return
    if(password == password_db):
        print(f"connecté en tant que :{name}" )
    cursor.close()


def delete():
    con = sqlite3.connect("./data/beecoin.db")
    cursor = con.cursor()
    idd = int(input("Entrez ID: "))
    query = "DELETE from USERS where ID = ?"
    result = cursor.execute(query, (idd))
    con.commit()
    cursor.close()
    if(result):
        print("One record deleted")
    else:
        print("Error")

while (True):
    print("1). Create User: ")
    print("2). Read Users: ")
    print("3). Update Users: ")
    print("4). Delete Users: ")
    print("5). Send")
    print("6). Exit ")
    ch = int(input("Entrez votre choix: "))
    if ch == 1:
        create()
    elif ch == 2:
        print("1). Read Single user")
        print("2). Read All users")
        choice = int(input("Entrez votre choix: "))
        if choice == 1:
            read_one()
        elif choice == 2:
            read_all()
        else: 
            print("Error")
    elif ch == 3:
        update()
    elif ch == 4:
        delete()
    elif ch == 5:
        send()
    elif ch == 6:
        break
    else:
        print("Entrez un choix correct")

